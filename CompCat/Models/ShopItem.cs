﻿using CompCat.Models.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CompCat.Models
{
    public class ShopItem
    {
        public Shop Shop { get; set; }
        public Item Item { get; set; }
        public int Price { get; set; }
    }
}