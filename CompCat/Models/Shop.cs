﻿using System.Collections.Generic;

namespace CompCat.Models
{
    public class Shop
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<ShopBuilding> Buildings { get; set; }
        public List<User> Admins { get; set; }
        public List<ShopItem> ShopItems { get; set; }
    }
}