﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CompCat.Models
{
    public class ShopBuilding
    {
        public int Id { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public int ShopId { get; set; }
        public Shop Shop { get; set; }
    }
}