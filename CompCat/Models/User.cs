﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CompCat.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Username { get; set; }
        public bool Banned { get; set; }
        public DateTime BanEndTime { get; set; }
        public string Image { get; set; }
        public List<Role> Roles { get; set; }
        public List<Shop> Shops { get; set; }
    }
}