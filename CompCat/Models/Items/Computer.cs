﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CompCat.Models.Items
{
    public class Computer : Item
    {
        public string CPU { get; set; }
        public int CoreNumber { get; set; }
        public int ClockFrequency { get; set; }
        public string DiscType { get; set; }
        public int DiskCapacity { get; set; }
        public int Memory { get; set; }
        public string VideoCardType { get; set; }
        public string VideoCard { get; set; }
        public string Color { get; set; }
    }
}