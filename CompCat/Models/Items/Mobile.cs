﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CompCat.Models.Items
{
    public class Mobile : Item
    {
        public decimal ScreenDiagonal { get; set; }
        public string Resolution { get; set; }
        public string ScreenTechnology { get; set; }
        public string CPU { get; set; }
        public int? ClockFrequency { get; set; }
        public string System { get; set; }
        public int? StorageCapacity { get; set; }
        public int? Memory { get; set; }
        public decimal? Camera { get; set; }
        public decimal? FrontCamera { get; set; }
        public int SimNumber { get; set; }
        public int BatteryCapacity { get; set; }
        public string Color { get; set; }
    }
}