﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CompCat.Models.Items
{
    public class Notebook : Item
    {
        public decimal ScreenDiagonal { get; set; }
        public string Resolution { get; set; }
        public string Matrix { get; set; }
        public string CPU { get; set; }
        public int ClockFrequency { get; set; }
        public string DiscType { get; set; }
        public int DiskCapacity { get; set; }
        public int Memory { get; set; }
        public string VideoCardType { get; set; }
        public string VideoCard { get; set; }
        public int BatteryCapacity { get; set; }
        public string CoverColor { get; set; }
        public string BodyColor { get; set; }
    }
}