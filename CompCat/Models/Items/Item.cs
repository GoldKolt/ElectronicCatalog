﻿using System.Collections.Generic;

namespace CompCat.Models.Items
{
    abstract public class Item
    {
        public int Id { get; set; }
        public string Company { get; set; }
        public string Model { get; set; }
        public int? Year { get; set; }
        public string Image { get; set; }
        public List<ShopItem> ShopItems { get; set; }
    }
}