﻿create table [dbo].[User] (
	[Id] int identity(1,1) not null primary key,
	[Email] nvarchar(255) not null,
	[Password] nvarchar(255) not null,
	[Username] nvarchar(255) not null,
	[Banned] bit not null default 0,
	[BanEndTime] datetime,
	[Image] nvarchar(255) not null default '~/Content/Images/Users/default.png'
)

create table [dbo].[Role] (
	[Id] int identity(1,1) not null primary key,
	[Name] nvarchar(50) not null
)

create table [dbo].[UserRole] (
	[UserId] int not null foreign key references [dbo].[User]([Id]) on update cascade on delete cascade,
	[RoleId] int not null foreign key references [dbo].[Role]([Id]) on update cascade on delete cascade
)

create table [dbo].[Shop] (
	[Id] int identity(1,1) not null primary key,
	[Name] nvarchar(255) not null
)

create table [dbo].[ShopBuilding] (
	[Id] int identity(1,1) not null primary key,
	[City] nvarchar(50) not null,
	[Address] nvarchar(255) not null,
	[ShopId]int not null foreign key references [dbo].[Shop]([Id]) on update cascade on delete cascade
)

create table [dbo].[Item] (
    [Id] int identity(1,1) not null primary key,
    [Company] nvarchar(100) not null,
    [Model] nvarchar(100) not null,
    [Year] int,
    [Image] nvarchar(255) not null default '~/Content/Images/Items/default.png'
);

create table [dbo].[ItemShop] (
	[ItemId] int not null foreign key references [dbo].[Item]([Id]) on update cascade on delete cascade,
	[ShopId] int not null foreign key references [dbo].[Shop]([Id]) on update cascade on delete cascade,
	[Price] numeric(9,2) not null
)

create table [dbo].[ShopAdmin] (
	[UserId] int not null foreign key references [dbo].[User]([Id]) on update cascade on delete cascade,
	[ShopId] int not null foreign key references [dbo].[Shop]([Id]) on update cascade on delete cascade
)

create table [dbo].[Computer] (
	[Id] int not null primary key,
	[CPU] nvarchar(255) not null,
	[CoreNumber] int not null,
    [ClockFrequency] int not null,
    [DiscType] nvarchar(50) not null,
    [DiscCapacity] int not null,
    [Memory] int not null,
    [VideoCardType] nvarchar(50) not null,
    [VideoCard] nvarchar(250) not null,
    [Color] nvarchar(50)
)

create table [dbo].[Mobile] (
    [Id] int not null primary key,
    [ScreenDiagonal] numeric(5,2) not null,
    [Resolution] nvarchar(20) not null,
    [ScreenTechnology] nvarchar(255) not null,
    [CPU] nvarchar(255),
    [ClockFrequency] int,
    [System] nvarchar(255),
    [StotageCapacity] int,
    [Memory] int,
    [Camera] numeric(5,2),
    [FrontCamera] numeric(5,2),
    [SimNumber] int not null,
    [BatteryCapacity] int not null,
    [Color] nvarchar(50)
);

create table [dbo].[Notebook] (
    [Id] int not null primary key,
    [ScreenDiagonal] numeric(5,2) not null,
    [Resolution] nvarchar(20) not null,
    [Matrix] nvarchar(255) not null,
    [CPU] nvarchar(255) not null,
    [ClockFrequency] int not null,
    [DiscType] nvarchar(50) not null,
    [DiscCapacity] int not null,
    [Memory] int not null,
    [VideocardType] nvarchar(50) not null,
    [VideoCard] nvarchar(255) not null,
    [BatteryCapacity] int not null,
    [CoverColor] nvarchar(50),
    [BodyColor] nvarchar(50)
);