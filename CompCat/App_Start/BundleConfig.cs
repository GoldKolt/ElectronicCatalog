﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace CompCat.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/Angular").Include(
                "~/Scripts/Angular/runtime.*",
                "~/Scripts/Angular/polyfills.*",
                "~/Scripts/Angular/styles.*",
                "~/Scripts/Angular/vendor.*",
                "~/Scripts/Angular/main.*"));
        }
    }
}