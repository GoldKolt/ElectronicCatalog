﻿using CompCat.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CompCat.DAL
{
    public class MSSqlUnitOfWork : IUnitOfWork
    {
        private readonly SqlConnection connection;
        private readonly SqlTransaction transaction;
        private IRepository<User> userRepository;

        public MSSqlUnitOfWork()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["Default Connection"].ConnectionString;
            connection = new SqlConnection(connectionString);
            connection.Open();
            transaction = connection.BeginTransaction();
        }

        public IRepository<User> UserRepository
        {
            get
            {
                if (userRepository == null)
                    userRepository = new MSSqlUserRepository(connection, transaction);
                return userRepository;
            }
        }

        public void Save()
        {
            try
            {
                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; 

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing) { }
                transaction.Dispose();
                connection.Dispose();
                disposedValue = true;
            }
        }

        ~MSSqlUnitOfWork()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

    }
}