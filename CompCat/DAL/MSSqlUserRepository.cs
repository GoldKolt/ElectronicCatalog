﻿using CompCat.Models;
using CompCat.Utils;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Security;
using System.Threading.Tasks;
using System.Web;

namespace CompCat.DAL
{
    public class MSSqlUserRepository : IRepository<User>
    {
        private readonly SqlConnection connection;
        private readonly SqlTransaction transaction;

        public MSSqlUserRepository(SqlConnection connection, SqlTransaction transaction)
        {
            this.connection = connection;
            this.transaction = transaction;
        }

        public async Task<bool> Create(User entity)
        {
            bool result = false;
            try
            {
                byte[] salt = HashGenerator.GetSalt();
                string query = string.Format("INSERT INTO [dbo].[User] VALUES (NULL, {0}, {1}, {2}, FALSE, NULL, {3})", entity.Email, HashGenerator.GetHash(entity.Password, salt), entity.Email, entity.Image);
                result = await Execute(query);
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
            return result;
        }

        public async Task<bool> Delete(int id)
        {
            bool result = false;
            try
            {
                string query = string.Format("DELETE FROM [dbo].[User] WHERE [id] = {0}", id);
                result = await Execute(query);
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
            return result;
        }

        public async Task<User> Get(int id)
        {
            User user = null;
            try
            {
                string query = string.Format("SELECT ([Email], [Username], [Banned], [BanEndTime], [Image]) FROM [dbo].[User] WHERE [id] = {0}", id);
                SqlCommand command = new SqlCommand(query, connection);
                SqlDataReader reader = await command.ExecuteReaderAsync();
                if (reader.HasRows)
                {
                    await reader.ReadAsync();
                    user = new User() { Email = reader.GetString(0), Username = reader.GetString(1), Banned = reader.GetBoolean(2), BanEndTime = reader.GetDateTime(3) };
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return user;
        }

        public async Task<IEnumerable<User>> GetAll()
        {
            IEnumerable<User> users = new List<User>();
            try
            {
                string query = string.Format("SELECT ([Email], [Username], [Banned], [BanEndTime], [Image]) FROM [dbo].[User]");
                SqlCommand command = new SqlCommand(query, connection);
                SqlDataReader reader = await command.ExecuteReaderAsync();
                if (reader.HasRows)
                {
                    while (await reader.ReadAsync())
                    {
                        User user = new User() { Email = reader.GetString(0), Username = reader.GetString(1), Banned = reader.GetBoolean(2), BanEndTime = reader.GetDateTime(3) };
                        users.Append(user);
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return users;
        }

        public async Task<bool> Update(User entity)
        {
            bool result = false;
            try
            {
                string query = string.Format("UPDATE [dbo].[User] SET [Email] = {0}, [Username] = {1}, [Image] = {2} WHERE [Id] = {3}", entity.Email, entity.Username, entity.Image, entity.Id);
                result = await Execute(query);
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
            return result;
        }

        public async Task<bool> BanUser(int id, DateTime BanTime)
        {
            bool result = false;
            try
            {
                string query = string.Format("UPDATE [dbo].[User] SET BanEndTime = {0} WHERE [Id] = {1}", BanTime, id);
                result = await Execute(query);
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
            return result;
        }

        public async Task<bool> ChangePassword(int id, string oldPassword, string newPassword)
        {
            bool result = false;
            string query = string.Format("SELECT [Password] FROM [dbo].[User] WHERE [Id] = {0}", id);
            using (SqlCommand command = new SqlCommand(query, connection))
            {
                SqlDataReader userReader = await command.ExecuteReaderAsync();
                if (userReader.HasRows)
                {
                    await userReader.ReadAsync();
                    string hash = userReader.GetString(0);
                    if (CheckPassword(hash, oldPassword))
                    {
                        query = string.Format("UPDATE [User] SET [Password] = {0} WHERE [Id] = {1}", HashGenerator.GetHash(newPassword, HashGenerator.GetSalt()), id);
                        result = await Execute(query);
                    }
                }
            }
            return result;
        }

        public async Task<User> Login(string Email, string password)
        {
            User user = null;
            string query = string.Format("SELECT ([Id], [Email], [Password], [UserName], [BanEndTime], [Image]) FROM [dbo].[User] WHERE [Email] = {0};", Email);
            using (SqlCommand command = new SqlCommand(query, connection))
            {
                SqlDataReader userReader = await command.ExecuteReaderAsync();
                if (userReader.HasRows)
                {
                    await userReader.ReadAsync();
                    string hash = userReader.GetString(2);
                    if (CheckPassword(hash, password))
                    {
                        user = new User() { Id = userReader.GetInt32(0), Email = userReader.GetString(1), Username = userReader.GetString(3), BanEndTime = userReader.GetDateTime(4), Image = userReader.GetString(5) };
                        user = await GetRoles(user);
                        user = await GetShops(user);
                    }
                }
            }
            return user;
        }

        private async Task<bool> Execute(string query)
        {
            using (SqlCommand command = new SqlCommand(query, connection, transaction))
            {
                int u = await command.ExecuteNonQueryAsync();
                if (u != 0)
                {
                    return true;
                }
            }
            return false;
        }

        private async Task<User> GetRoles(User user)
        {
            string query = string.Format("SELECT [Role].[Id], [Role].[Name] FROM [Role] INNER JOIN [UserRole] ON [Role].[Id] = [UserRole].[RoleId] WHERE [UserRole].[UserId] = {0}", user.Id);
            using (SqlCommand command = new SqlCommand(query, connection))
            {
                var reader = await command.ExecuteReaderAsync();
                if (reader.HasRows)
                {
                    while(await reader.ReadAsync())
                    {
                        Role role = new Role() { Id = reader.GetInt32(0), Name = reader.GetString(1) };
                        user.Roles.Add(role);
                    }
                }
            }
            return user;
        }

        private async Task<User> GetShops(User user)
        {
            string query = string.Format("SELECT [Shop].[Id], [Shop].[Name] FROM [Shop] INNER JOIN [ShopAdmin] ON [Shop].[Id] = [ShopAdmin].[ShopId] WHERE [UserRole].[UserId] = {0}", user.Id);
            using (SqlCommand command = new SqlCommand(query, connection))
            {
                var reader = await command.ExecuteReaderAsync();
                if (reader.HasRows)
                {
                    while (await reader.ReadAsync())
                    {
                        Shop shop = new Shop() { Id = reader.GetInt32(0), Name = reader.GetString(1) };
                        user.Shops.Add(shop);
                    }
                }
            }
            return user;
        }

        private bool CheckPassword(string hash, string toCheck)
        {
            byte[] passByte = Convert.FromBase64String(hash);
            byte[] salt = new byte[64];
            Array.Copy(passByte, 64, salt, 0, 64);
            return (HashGenerator.GetHash(toCheck, salt) == hash);
        }
    }
}