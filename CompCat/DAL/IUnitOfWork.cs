﻿using System;

namespace CompCat.DAL
{
    public interface IUnitOfWork : IDisposable
    {
        void Save();
    }
}
