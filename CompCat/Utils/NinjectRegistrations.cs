﻿using Ninject.Modules;
using CompCat.DAL;

namespace CompCat.Utils
{
    public class NinjectRegistrations : NinjectModule
    {
        public override void Load()
        {
            Bind<IUnitOfWork>().To<MSSqlUnitOfWork>();
        }
    }
}