﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace CompCat.Utils
{
    public static class HashGenerator
    {
        private const int SaltSize = 64;

        public static string GetHash(string data, byte[] salt)
        {
            byte[] hashBytes;
            using (var rfc = new Rfc2898DeriveBytes(data, salt, 4096))
            {
                hashBytes = rfc.GetBytes(64);
            }
            byte[] resultBytes = new byte[hashBytes.Length + salt.Length];
            hashBytes.CopyTo(resultBytes, 0);
            salt.CopyTo(resultBytes, hashBytes.Length);
            return Convert.ToBase64String(resultBytes);
        }

        public static byte[] GetSalt()
        {
            var randomBytes = new byte[SaltSize];
            using (var rng = new RNGCryptoServiceProvider())
            {
                rng.GetBytes(randomBytes);
            }
            return randomBytes;
        }
    }
}